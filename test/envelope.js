
'use strict';

import tap         from 'tap';
import sinon       from 'sinon';
import Envelope    from '../lib/envelope';

function generateMockDataWithCode( code ) {
    return {
        errorCode: code,
        errorType: 'cheat error',
        errorDetails: [ 'the system is down!' ],
    };
}

function isClass( func ) {
    return typeof func === 'function'
        && !func.hasOwnProperty( 'arguments' );
}

tap.test( 'API Envelope', ( suite ) => {

    tap.test( 'has static Status Codes', ( assert ) => {

        // create a sandbox for sinon ease
        let sandbox = sinon.sandbox.create();

        // create Stub of Envelope constructor
        let EnvelopeStub = sandbox.spy( () => {
            return sinon.createStubInstance( Envelope );
        } );

        tap.test( 'class verification', ( assert ) => {
            assert.ok( isClass( Envelope ), 'Envelope is a Class' );
            assert.end();
        } );

        tap.test( 'statusCodes()', ( assert ) => {
            assert.ok( Envelope.statusCodes(), 'statusCodes is static' );
            assert.end();
        } );

        tap.test( 'statusCode( code )', ( assert ) => {
            let code = 400;
            assert.deepEqual( Envelope.statusCode( code ), Envelope.statusCodes()[ code ], 'static statusCode( code ) returns correct code' );
            assert.end();
        } );

        tap.test( 'class instantiation check', ( assert ) => {
            assert.notOk( EnvelopeStub.called, 'Envelope constructor hasn\'t been called.' );
            assert.equal( EnvelopeStub.callCount, 0, 'Envelope constructor has not been called once.' );

            let envelope = new EnvelopeStub();

            assert.ok( EnvelopeStub.called, 'Envelope constructor has been called.' );
            assert.equal( EnvelopeStub.callCount, 1, 'Envelope constructor has been called once.' );
            assert.throws(
                () => {
                    return envelope.statusCodes();
                },
                'envelope.statusCodes is not a function',
                'static members can\'t be called off instance'
            );
            assert.end();
        } );

        // Reset Envelope constructor Stub
        sandbox.restore();

        // end subtest
        assert.end();
    } );

    tap.test( 'envelope.constructor()', ( assert ) => {
        let envelope = new Envelope();
        assert.equal( envelope.meta.code, 200, 'status code is 200.' );
        assert.notOk( envelope.meta.errorType, 'meta.errorType object does not exist.' );
        assert.notOk( envelope.meta.errorDetail, 'meta.errorDetail object does not exist.' );
        assert.notOk( envelope.response, 'response object does not exist.' );
        assert.end();
    } );

    tap.test( 'envelope meta getter', ( assert ) => {
        let envelope = new Envelope(),
            metadata = envelope.metaData;

        assert.deepEqual( envelope.meta, metadata, 'meta getter returns metadata.' );
        assert.end();
    } );


    tap.test( 'envelope response getter', ( assert ) => {
        let envelope = new Envelope(),
            response;

        envelope.success();
        response = envelope.response;

        assert.deepEqual( envelope.response, response, 'response getter returns response.' );
        assert.end();
    } );

    tap.test( 'envelope notifications getter', ( assert ) => {
        let envelope = new Envelope(),
            msgData  = {
                level:'info',
                msg:  'Foo!',
            },
            notifications;

        envelope.notify( msgData );
        notifications = envelope.notificationsData;

        assert.deepEqual( envelope.notifications, notifications, 'notifications getter returns notifications.' );
        assert.end();
    } );

    tap.test( 'envelope.reset()', ( assert ) => {

        tap.test( 'resets instance meta', ( assert ) => {
            let envelope = new Envelope();

            envelope.meta = {
                foo: 'bar',
            };
            envelope.foobar = [ 'one', 2, 'thareee!' ];

            envelope.reset();

            assert.ok( envelope.meta, 'meta object exists.' );
            assert.equal( envelope.meta.code, 200, 'status code is 200.' );

            assert.notOk( envelope.meta.foobar, 'meta.errorType object does not exist.' );
            assert.notOk( envelope.foobar, 'foobar object does not exist.' );

            assert.notOk( envelope.meta.errorType, 'meta.errorType object does not exist.' );
            assert.notOk( envelope.meta.errorDetail, 'meta.errorDetail object does not exist.' );
            assert.notOk( envelope.response, 'response object does not exist.' );
            assert.end();
        } );

        tap.test( 'resets prior states', ( assert ) => {
            let envelope = new Envelope(),
                mockResponse = {
                foo:'bar',
            };

            envelope.response = mockResponse;

            assert.ok( envelope.meta, 'meta object exists.' );
            assert.equal( envelope.meta.code, 200, 'status code is 200.' );
            assert.ok( envelope.response, 'response object exists' );

            envelope.reset();

            assert.ok( envelope.meta, 'meta object exists.' );
            assert.equal( envelope.meta.code, 200, 'status code is 200.' );
            assert.notOk( envelope.response, 'response object does not exist' );

            assert.end();
        } );

        assert.end();
    } );

    tap.test( 'envelope.success()', ( assert ) => {
        let envelope = new Envelope();

        tap.test( 'returns empty success envelope', ( assert ) => {
            envelope.success();

            assert.equal( envelope.meta.code, 200, 'returns a 200 status code' );
            assert.deepEqual( envelope.response, {}, 'response object is empty by default.' );
            assert.end();
        } );

        tap.test( 'returns expected success envelope', ( assert ) => {
            let mockData = {
                foo: 'bar',
            };
            envelope.success( mockData );

            assert.equal( envelope.meta.code, 200, 'returns a 200 status code' );
            assert.deepEqual( envelope.response, mockData, 'response object is same as mock data.' );
            assert.end();
        } );

        tap.test( 'returns expected success envelope with status code', ( assert ) => {
            let mockCode = 201,
                mockData = {
                foo: 'bar',
            };
            envelope.success( mockData, mockCode );

            assert.equal( envelope.meta.code, mockCode, `returns a ${mockCode} status code` );
            assert.deepEqual( envelope.response, mockData, 'response object is same as mock data.' );
            assert.end();
        } );
        assert.end();
    } );

    tap.test( 'envelope.error()', ( assert ) => {

        let envelope = new Envelope(),
            mockData = {
                errorType: 'lil_brudder',
                errorDetails: [ 'fhqwhgads' ],
            };

        tap.test( 'generic 400 error envelope nothing passed', ( assert ) => {
            envelope.error();
            assert.equal( envelope.meta.code, 400, 'returns a 400 status code' );
            assert.equal( envelope.meta.errorType, Envelope.statusCodes()[ 400 ].errorType, 'returns expcted errorType' );
            assert.deepEqual( envelope.meta.errorDetail, Envelope.statusCodes()[ 400 ].errorDetail, 'returns expected errorDetail' );
            assert.end();
        } );

        tap.test( 'generic 400 error envelope', ( assert ) => {
            envelope.error( 400 );
            assert.equal( envelope.meta.code, 400, 'returns a 400 status code' );
            assert.equal( envelope.meta.errorType, Envelope.statusCodes()[ 400 ].errorType, 'returns expcted errorType' );
            assert.deepEqual( envelope.meta.errorDetail, Envelope.statusCodes()[ 400 ].errorDetail, 'returns expected errorDetail' );
            assert.end();
        } );

        tap.test( 'custom 400 error envelope with supplied code', ( assert ) => {
            envelope.error( 400, mockData );
            assert.equal( envelope.meta.code, 400, 'returns a 400 status code' );
            assert.equal( envelope.meta.errorType, mockData.errorType, 'returns expcted errorType' );
            assert.deepEqual( envelope.meta.errorDetail, mockData.errorDetail, 'returns expected errorDetail' );
            assert.end();
        } );

        tap.test( 'custom 400 error envelope no supplied code', ( assert ) => {
            let mock = generateMockDataWithCode( 400 );
            envelope.error( mock );
            assert.equal( envelope.meta.code, 400, 'returns a 400 status code' );
            assert.equal( envelope.meta.errorType, mock.errorType, 'returns expcted errorType' );
            assert.deepEqual( envelope.meta.errorDetail, mock.errorDetail, 'returns expected errorDetail' );
            assert.end();
        } );

        tap.test( 'generic 401 error envelope', ( assert ) => {
            envelope.error( 401 );
            assert.equal( envelope.meta.code, 401, 'returns a 401 status code' );
            assert.equal( envelope.meta.errorType, Envelope.statusCodes()[ 401 ].errorType, 'returns expcted errorType' );
            assert.deepEqual( envelope.meta.errorDetail, Envelope.statusCodes()[ 401 ].errorDetail, 'returns expected errorDetail' );
            assert.end();
        } );

        tap.test( 'custom 401 error envelope with supplied code', ( assert ) => {
            envelope.error( 401, mockData );
            assert.equal( envelope.meta.code, 401, 'returns a 401 status code' );
            assert.equal( envelope.meta.errorType, mockData.errorType, 'returns expcted errorType' );
            assert.deepEqual( envelope.meta.errorDetail, mockData.errorDetail, 'returns expected errorDetail' );
            assert.end();
        } );

        tap.test( 'custom 401 error envelope no supplied code', ( assert ) => {
            let mock = generateMockDataWithCode( 401 );
            envelope.error( mock );
            assert.equal( envelope.meta.code, 401, 'returns a 401 status code' );
            assert.equal( envelope.meta.errorType, mock.errorType, 'returns expcted errorType' );
            assert.deepEqual( envelope.meta.errorDetail, mock.errorDetail, 'returns expected errorDetail' );
            assert.end();
        } );

        tap.test( 'generic 403 error envelope', ( assert ) => {
            envelope.error( 403 );
            assert.equal( envelope.meta.code, 403, 'returns a 403 status code' );
            assert.equal( envelope.meta.errorType, Envelope.statusCodes()[ 403 ].errorType, 'returns expcted errorType' );
            assert.deepEqual( envelope.meta.errorDetail, Envelope.statusCodes()[ 403 ].errorDetail, 'returns expected errorDetail' );
            assert.end();
        } );

        tap.test( 'custom 403 error envelope with supplied code', ( assert ) => {
            envelope.error( 403, mockData );
            assert.equal( envelope.meta.code, 403, 'returns a 403 status code' );
            assert.equal( envelope.meta.errorType, mockData.errorType, 'returns expcted errorType' );
            assert.deepEqual( envelope.meta.errorDetail, mockData.errorDetail, 'returns expected errorDetail' );
            assert.end();
        } );

        tap.test( 'custom 403 error envelope no supplied code', ( assert ) => {
            let mock = generateMockDataWithCode( 403 );
            envelope.error( mock );
            assert.equal( envelope.meta.code, 403, 'returns a 403 status code' );
            assert.equal( envelope.meta.errorType, mock.errorType, 'returns expcted errorType' );
            assert.deepEqual( envelope.meta.errorDetail, mock.errorDetail, 'returns expected errorDetail' );
            assert.end();
        } );

        tap.test( 'generic 404 error envelope', ( assert ) => {
            envelope.error( 404 );
            assert.equal( envelope.meta.code, 404, 'returns a 404 status code' );
            assert.equal( envelope.meta.errorType, Envelope.statusCodes()[ 404 ].errorType, 'returns expcted errorType' );
            assert.deepEqual( envelope.meta.errorDetail, Envelope.statusCodes()[ 404 ].errorDetail, 'returns expected errorDetail' );
            assert.end();
        } );

        tap.test( 'custom 404 error envelope with supplied code', ( assert ) => {
            envelope.error( 404, mockData );
            assert.equal( envelope.meta.code, 404, 'returns a 404 status code' );
            assert.equal( envelope.meta.errorType, mockData.errorType, 'returns expcted errorType' );
            assert.deepEqual( envelope.meta.errorDetail, mockData.errorDetail, 'returns expected errorDetail' );
            assert.end();
        } );

        tap.test( 'custom 404 error envelope no supplied code', ( assert ) => {
            let mock = generateMockDataWithCode( 404 );
            envelope.error( mock );
            assert.equal( envelope.meta.code, 404, 'returns a 404 status code' );
            assert.equal( envelope.meta.errorType, mock.errorType, 'returns expcted errorType' );
            assert.deepEqual( envelope.meta.errorDetail, mock.errorDetail, 'returns expected errorDetail' );
            assert.end();
        } );

        tap.test( 'generic 405 error envelope', ( assert ) => {
            envelope.error( 405 );
            assert.equal( envelope.meta.code, 405, 'returns a 405 status code' );
            assert.equal( envelope.meta.errorType, Envelope.statusCodes()[ 405 ].errorType, 'returns expcted errorType' );
            assert.deepEqual( envelope.meta.errorDetail, Envelope.statusCodes()[ 405 ].errorDetail, 'returns expected errorDetail' );
            assert.end();
        } );

        tap.test( 'custom 405 error envelope with supplied code', ( assert ) => {
            envelope.error( 405, mockData );
            assert.equal( envelope.meta.code, 405, 'returns a 405 status code' );
            assert.equal( envelope.meta.errorType, mockData.errorType, 'returns expcted errorType' );
            assert.deepEqual( envelope.meta.errorDetail, mockData.errorDetail, 'returns expected errorDetail' );
            assert.end();
        } );

        tap.test( 'custom 405 error envelope no supplied code', ( assert ) => {
            let mock = generateMockDataWithCode( 405 );
            envelope.error( mock );
            assert.equal( envelope.meta.code, 405, 'returns a 405 status code' );
            assert.equal( envelope.meta.errorType, mock.errorType, 'returns expcted errorType' );
            assert.deepEqual( envelope.meta.errorDetail, mock.errorDetail, 'returns expected errorDetail' );
            assert.end();
        } );

        tap.test( 'generic 409 error envelope', ( assert ) => {
            envelope.error( 409 );
            assert.equal( envelope.meta.code, 409, 'returns a 409 status code' );
            assert.equal( envelope.meta.errorType, Envelope.statusCodes()[ 409 ].errorType, 'returns expcted errorType' );
            assert.deepEqual( envelope.meta.errorDetail, Envelope.statusCodes()[ 409 ].errorDetail, 'returns expected errorDetail' );
            assert.end();
        } );

        tap.test( 'custom 409 error envelope with supplied code', ( assert ) => {
            envelope.error( 409, mockData );
            assert.equal( envelope.meta.code, 409, 'returns a 409 status code' );
            assert.equal( envelope.meta.errorType, mockData.errorType, 'returns expcted errorType' );
            assert.deepEqual( envelope.meta.errorDetail, mockData.errorDetail, 'returns expected errorDetail' );
            assert.end();
        } );

        tap.test( 'custom 409 error envelope no supplied code', ( assert ) => {
            let mock = generateMockDataWithCode( 409 );
            envelope.error( mock );
            assert.equal( envelope.meta.code, 409, 'returns a 409 status code' );
            assert.equal( envelope.meta.errorType, mock.errorType, 'returns expcted errorType' );
            assert.deepEqual( envelope.meta.errorDetail, mock.errorDetail, 'returns expected errorDetail' );
            assert.end();
        } );

        tap.test( 'generic 451 error envelope', ( assert ) => {
            envelope.error( 451 );
            assert.equal( envelope.meta.code, 451, 'returns a 451 status code' );
            assert.equal( envelope.meta.errorType, Envelope.statusCodes()[ 451 ].errorType, 'returns expcted errorType' );
            assert.deepEqual( envelope.meta.errorDetail, Envelope.statusCodes()[ 451 ].errorDetail, 'returns expected errorDetail' );
            assert.end();
        } );

        tap.test( 'custom 451 error envelope with supplied code', ( assert ) => {
            envelope.error( 451, mockData );
            assert.equal( envelope.meta.code, 451, 'returns a 451 status code' );
            assert.equal( envelope.meta.errorType, mockData.errorType, 'returns expcted errorType' );
            assert.deepEqual( envelope.meta.errorDetail, mockData.errorDetail, 'returns expected errorDetail' );
            assert.end();
        } );

        tap.test( 'custom 451 error envelope no supplied code', ( assert ) => {
            let mock = generateMockDataWithCode( 451 );
            envelope.error( mock );
            assert.equal( envelope.meta.code, 451, 'returns a 451 status code' );
            assert.equal( envelope.meta.errorType, mock.errorType, 'returns expcted errorType' );
            assert.deepEqual( envelope.meta.errorDetail, mock.errorDetail, 'returns expected errorDetail' );
            assert.end();
        } );

        tap.test( 'generic 500 error envelope', ( assert ) => {
            envelope.error( 500 );
            assert.equal( envelope.meta.code, 500, 'returns a 500 status code' );
            assert.equal( envelope.meta.errorType, Envelope.statusCodes()[ 500 ].errorType, 'returns expcted errorType' );
            assert.deepEqual( envelope.meta.errorDetail, Envelope.statusCodes()[ 500 ].errorDetail, 'returns expected errorDetail' );
            assert.end();
        } );

        tap.test( 'custom 500 error envelope with supplied code', ( assert ) => {
            envelope.error( 500, mockData );
            assert.equal( envelope.meta.code, 500, 'returns a 500 status code' );
            assert.equal( envelope.meta.errorType, mockData.errorType, 'returns expcted errorType' );
            assert.deepEqual( envelope.meta.errorDetail, mockData.errorDetail, 'returns expected errorDetail' );
            assert.end();
        } );

        tap.test( 'custom 500 error envelope no supplied code', ( assert ) => {
            let mock = generateMockDataWithCode( 500 );
            envelope.error( mock );
            assert.equal( envelope.meta.code, 500, 'returns a 500 status code' );
            assert.equal( envelope.meta.errorType, mock.errorType, 'returns expcted errorType' );
            assert.deepEqual( envelope.meta.errorDetail, mock.errorDetail, 'returns expected errorDetail' );
            assert.end();
        } );

        tap.test( 'Error Code as String parses properly', ( assert ) => {
            envelope.error( '400' );
            assert.equal( envelope.meta.code, 400, 'returns a 400 status code' );
            assert.equal( envelope.meta.errorType, Envelope.statusCodes()[ 400 ].errorType, 'returns expcted errorType' );
            assert.deepEqual( envelope.meta.errorDetail, Envelope.statusCodes()[ 400 ].errorDetail, 'returns expected errorDetail' );
            assert.end();
        } );

        tap.test( 'Undefined Error Code returns proper meta object: code only argument', ( assert ) => {
            let code = 999;
            envelope.error( code );
            assert.equal( envelope.meta.code, code, `returns a ${code} status code` );
            assert.notOk( envelope.meta.errorType, 'errorType not returned' );
            assert.notOk( envelope.meta.errorDetail, 'errorDetail not returned' );
            assert.end();
        } );

        tap.test( 'Undefined Error Code returns proper meta object: code and options arguments', ( assert ) => {
            let code = 999;
            envelope.error( code, { foo: 'bar' } );
            assert.equal( envelope.meta.code, code, `returns a ${code} status code` );
            assert.notOk( envelope.meta.errorType, 'errorType not returned' );
            assert.notOk( envelope.meta.errorDetail, 'errorDetail not returned' );
            assert.end();
        } );

        tap.test( 'Undefined Error Code returns proper meta object: only options argument', ( assert ) => {
            let options = {
                errorCode: 999,
            };
            envelope.error( options );
            assert.equal( envelope.meta.code, options.errorCode, `returns a ${options.errorCode} status code` );
            assert.notOk( envelope.meta.errorType, 'errorType not returned' );
            assert.notOk( envelope.meta.errorDetail, 'errorDetail not returned' );
            assert.end();
        } );

        tap.test( 'Undefined Error Code returns proper meta object: only options argument no code', ( assert ) => {
            let options = {
                foo: 999,
            };
            envelope.error( options );
            assert.equal( envelope.meta.code, 400, 'returns a 400 status code' );
            assert.equal( envelope.meta.errorType, Envelope.statusCodes()[ 400 ].errorType, 'returns expcted errorType' );
            assert.deepEqual( envelope.meta.errorDetail, Envelope.statusCodes()[ 400 ].errorDetail, 'returns expected errorDetail' );
            assert.end();
        } );

        tap.test( 'error envelope with success code', ( assert ) => {
            envelope.error( 200 );
            assert.equal( envelope.meta.code, 400, 'defaults to a 400 status code' );
            assert.equal( envelope.meta.errorType, Envelope.statusCodes()[ 400 ].errorType, 'returns expcted errorType' );
            assert.deepEqual( envelope.meta.errorDetail, Envelope.statusCodes()[ 400 ].errorDetail, 'returns expected errorDetail' );
            assert.end();
        } );

        assert.end();
    } );

    tap.test( 'envelope.notify()', ( assert ) => {

        tap.test( 'notification array append', ( assert ) => {
            let envelope = new Envelope(),
                mockArr0 = [
                    {
                        type:'warn',
                        msg: 'Foo!',
                    },
                ],
                mockArr1 = [
                    {
                        type:'info',
                        msg: 'Hodor!',
                    },
                ],
                mockList;

            mockList = mockArr0.concat( mockArr1 );

            envelope.notify( mockArr0 );
            assert.deepEqual( envelope.notificationsData, mockArr0, 'sets notifications array' );

            envelope.notify( mockArr1 );
            assert.deepEqual( envelope.notificationsData, mockList, 'appends to notifications array' );

            assert.end();
        } );

        tap.test( 'notification array overwrite', ( assert ) => {
            let envelope = new Envelope(),
                mockArr0 = [
                    {
                        type:'warn',
                        msg: 'Foo!',
                    },
                ],
                mockArr1 = [
                    {
                        type:'info',
                        msg: 'Hodor!',
                    },
                ];

            envelope.notify( mockArr0, true );
            assert.deepEqual( envelope.notificationsData, mockArr0, 'sets notifications array' );

            envelope.notify( mockArr1, true );
            assert.deepEqual( envelope.notificationsData, mockArr1, 'overwrites notifications array' );

            assert.end();
        } );

        tap.test( 'notification object append', ( assert ) => {
            let envelope = new Envelope(),
                mockObj0 = {
                    type:'warn',
                    msg: 'Foo!',
                },
                mockObj1 = {
                    type:'info',
                    msg: 'Hodor!',
                },
                mockList = [ mockObj0, mockObj1 ];

            envelope.notify( mockObj0 );
            assert.deepEqual( envelope.notificationsData, [ mockObj0 ], 'sets notifications array' );

            envelope.notify( mockObj1 );
            assert.deepEqual( envelope.notificationsData, mockList, 'appends to notifications array' );

            assert.end();
        } );

        tap.test( 'notification object overwrite', ( assert ) => {
            let envelope = new Envelope(),
                mockObj0 = {
                    type:'warn',
                    msg: 'Foo!',
                },
                mockObj1 = {
                    type:'info',
                    msg: 'Hodor!',
                };


            envelope.notify( mockObj0, true );
            assert.deepEqual( envelope.notificationsData, [ mockObj0 ], 'sets notifications array' );

            envelope.notify( mockObj1, true );
            assert.deepEqual( envelope.notificationsData, [ mockObj1 ], 'overwrites notifications array' );

            assert.end();
        } );

        assert.end();
    } );

    suite.end();
} );
