# Code Style and Formatting
 - Use ES6.
 - Lint your code, there's a eslint configuration file provided.
 - format your code inline with the existing style; there's a jscs formatter 
 configuration file to assist with this.
 
# Tests are not optional
 - provide tests asserting your changes.
 - run coverage for your tests
