/**
 * API Envelope
 *
 * Constructs a configurable Data Envelope to return
 * a standardized JSON payload for XHR/Fetch requests to an API.
 *
 * The intent is to follow standards and best practices.
 * This Envelope is modeled after Foursquare's:
 * @link https://developer.foursquare.com/overview/responses
 *
 * @example
 * {
 *     "meta": {
 *         "code": 200,
 *         "errorType": "abc",
 *         "errorDetail": []
 *     },
 *     "notifications": [{ ... },...],
 *     "response": { ... }
 * }
 */

'use strict';

/**
 * Default Success Code
 *
 * sets the default success code
 *
 * @type {Number}
 */
const SUCCESS_CODE_DEFAULT = 200;


/**
 * Default Error Code
 *
 * sets the default error code
 *
 * @type {Number}
 */
const ERROR_CODE_DEFAULT = 400;

/**
 * Status Codes
 *
 * A collection of default status codes and their respective
 * default errorType and details.
 *
 * @type {Object}
 */
const STATUS_CODES = {
    200: {
        code: 200,
    },
    400: {
        code: 400,
        errorType: 'param_error',
        errorDetails: [ 'A required parameter was missing or a parameter was malformed.' ],
    },
    401: {
        code: 401,
        errorType: 'invalid_auth',
        errorDetails: [ 'OAuth token was not provided or was invalid.' ],
    },
    403: {
        code: 403,
        errorType: 'not_authorized',
        errorDetails: [ 'User is not authorized to take this action.' ],
    },
    404: {
        code: 404,
        errorType: 'endpoint_error',
        errorDetails: [ 'The requested path does not exist.' ],
    },
    405: {
        code: 405,
        errorType: 'not_allowed',
        errorDetails: [ 'Attempting to use POST with a GET-only endpoint, or vice-versa.' ],
    },
    409: {
        code: 409,
        errorType: 'conflict',
        errorDetails: [ 'The request could not be completed as it is.' ],
    },
    451: {
        code: 451,
        errorType: 'censored',
        errorDetails: [ 'The request could not be completed due to legal reasons.' ],
    },
    500: {
        code: 500,
        errorType: 'internal_error',
        errorDetails: [ 'The server is experiencing melancholy.' ],
    },
};

function retrieveErrorType( code ) {
    if( STATUS_CODES[ code ] ) {
        return STATUS_CODES[ code ].errorType;
    }

    return '';
}

function retrieveErrorDetails( code ) {
    if( STATUS_CODES[ code ] ) {
        return STATUS_CODES[ code ].errorDetails;
    }

    return [];
}

class Envelope {

    /**
     * Creates a new Envelope
     * @constructor
     */
    constructor() {
        this.meta          = {};
        this.response      = {};
        this.notifications = [];

        this.reset();
    }

    /**
     * Static method to return module constant STATUS_CODES
     *
     * @return {Object} Module constant STATUS_CODES
     */
    static statusCodes() {
        return STATUS_CODES;
    }

    /**
     * Static method to return specific module constant STATUS_CODES
     *
     * @param  {Number} code   status code
     * @return {Object} Module constant STATUS_CODES[ code ]
     */
    static statusCode( code ) {
        return STATUS_CODES[ code ];
    }

    /**
     * Getter: metaData
     * gets the value of the instance meta object
     *
     * @return {Object} meta object
     */
    get metaData() {
        return this.meta;
    }

    /**
     * Getter: notificationsData
     *
     * @return {Array} notifications list
     */
    get notificationsData() {
        return this.notifications;
    }

    /**
     * Getter: responseData
     *
     * @return {*} envelope response data
     */
    get responseData() {
        return this.response;
    }

    /**
     * Reset Envelope to default state.
     *
     * Resets/initializes Envelope to a default state of generic success.
     *
     * @return {Envelope} Envelope in default success state
     * @example
     * {
     *     meta: {
     *         code: 200
     *     }
     * }
     */
    reset() {

        for( let prop in this ) {
            /* istanbul ignore else */
            if( this.hasOwnProperty( prop ) ) {
                delete this[ prop ];
            }
        }

        this.meta          = STATUS_CODES[ SUCCESS_CODE_DEFAULT ];
        this.notifications = [];
    }

    /**
     * Creates an Error Envelope
     *
     * creates an error envelope using supplied code and options,
     * defaulting to a 400 if no code supplied.
     *
     * Error Envelopes do not return a Response object.
     *
     * @param  {String|Number}  code    status code [optional]
     * @param  {Object}         options object of error options [optional]
     * @return {Envelope}       Error   Envelope
     */
    error( code, options = {} ) {
        let errCode, errType, errDetails;

        // reset envelope
        this.reset();

        if( arguments.length < 2 ) {
            // only one argument supplied
            if(
                ( typeof code === 'number' ) ||
                ( typeof code === 'string' )
            ) {
                errCode    = ( typeof code === 'string' ) ? parseInt( code, 10 ) : code;
                errType    = retrieveErrorType( errCode );
                errDetails = retrieveErrorDetails( errCode );
            }
            else {
                if( arguments.length !== 0 ) {
                    // `code` was missing, see if its in 'options' object (in this case `code`)
                    let { errorCode, errorType, errorDetails } = code;

                    // if `errorCode` not found default to 400
                    errCode    = errorCode    || ERROR_CODE_DEFAULT;
                    errType    = errorType    || retrieveErrorType( errCode );
                    errDetails = errorDetails || retrieveErrorDetails( errCode );
                }
                else {
                    // nothing supplied, use defaults.
                    errCode    = ERROR_CODE_DEFAULT;
                    errType    = retrieveErrorType( errCode );
                    errDetails = retrieveErrorDetails( errCode );
                }
            }
        }
        else {
            // `code` and `options` arguements supplied
            let { errorType, errorDetails } = options;
            errCode    = code;
            errType    = errorType    || retrieveErrorType( errCode );
            errDetails = errorDetails || retrieveErrorDetails( errCode );
        }

        // remove success data
        delete this.response;

        // errCode should always be defined, set via `reset()`
        this.meta.code = errCode;


        if( this.meta.code === SUCCESS_CODE_DEFAULT ) {
            // a success code was supplied, use generic error (400)
            this.meta.code = ERROR_CODE_DEFAULT;
            errType = STATUS_CODES[ ERROR_CODE_DEFAULT ].errorType;
            errDetails = STATUS_CODES[ ERROR_CODE_DEFAULT ].errorDetails;
        }

        if( errType !== '' ) {
            // errType is defined, set meta.errorType
            this.meta.errorType = errType;
        }
        else {
            this.meta.errorType = '';
        }

        if( errDetails.length > 0 ) {
            // errDetails is defined, set meta.errorDetails
            this.meta.errorDetails = errDetails;
        }
        else {
            this.meta.errorDetails = [];
        }

        return this;
    }

    /**
     * Create a Success Envelope
     *
     * creates a success envelope using supplied code and data,
     * defaulting to a 200 if no code supplied.
     *
     * Success Envelopes do not return meta.errorType or meta.errorDetail
     *
     * @param  {Object}  data    success data [optional]
     * @param  {Number}  code    status code [optional]
     * @return {Envelope}               [description]
     */
    success( data = {}, code = SUCCESS_CODE_DEFAULT ) {

        // reset envelope
        this.reset();

        // set success data
        this.response  = data;
        this.meta.code = code;

        return this;
    }

    /**
     * Create a Notification entry in the envelope
     *
     * appends or overwrites notification data to the envelope.
     *
     * @param  {Object|Array} notificationData object/collection of objects containing notification data
     * @param  {Boolean} overwrite toggle to append/overwrite to notifications list defaults to append(false)
     * @return {[type]}                  [description]
     */
    notify( notificationData, overwrite = false ) {

        if( Array.isArray( notificationData ) ) {
            // collection of objects - hopefully.
            // check on overwrite flag to insert into existing or not.
            if( !overwrite ) {
                notificationData.forEach(
                    ( data ) => {
                        this.notifications.push( data );
                    },
                    this
                );
            }
            else {
                this.notifications = notificationData.slice();
            }
        }
        else {
            // object to append
            if( !overwrite ) {
                this.notifications.push( notificationData );
            }
            else {
                this.notifications = [ notificationData ];
            }
        }

        return this;
    }

}

export default Envelope;
